<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosGraduacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_graduacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_pos',255);
            $table->string('instituicao',100);
            $table->string('grau');
            $table->string('cidade')->nullable();
            $table->string('estado')->nullable();
            $table->string('url');
            $table->date('data_fim_inscricao')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_graduacoes');
    }
}
