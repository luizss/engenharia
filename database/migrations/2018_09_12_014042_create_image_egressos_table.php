<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageEgressosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_egressos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image',255);
            $table->integer('egresso_id')->unsigned();
            $table->foreign('egresso_id')->references('id')->on('egressos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_egressos');
    }
}
