@extends('layouts.admin')

@section('page-wrapper')
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
					Eventos - <small>Lista</small>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				@if(isset($eventos) && count($eventos) > 0)
				@if(Session::has('msg-warning'))
					<div class="alert alert-warning">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('msg-warning') }}
					</div>
				@endif
					<div class="table-responsive">
						<table class="table tb-pane">
							<tr>
								<th>Nome do evento</th>
								<th>url</th>
							</tr>	
							<tbody>
								@foreach($eventos as $evento)
									<tr>
										<td><a href="{{ route('dados-evento',$evento->id) }}">{{ $evento->nome }}</a></td>
										<td>{{ $evento->url }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@else
					<div class="alert alert-warning">Não há eventos cadastrados</div>	
				@endif
			</div>
		</div>
	</div>
@endsection