@extends('layouts.admin')

@section('page-wrapper')
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
					Evento - <small>Dados</small>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				@if(Session::has('msg-sucess'))
					<div class="alert alert-success">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('msg-sucess') }}
					</div>
				@endif

				@if(Session::has('msg-warning'))
					<div class="alert alert-warning">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('msg-warning') }}
					</div>
				@endif

				<div class="panel panel-default">
					<div class="panel-body">
						<p><b>Nome do evento: </b>{{ $evento->nome }}</p>
						@if($evento->url != null)
							<p><b>Link: </b>{{ $evento->url }}</p>
						@endif
						@if($evento->cidade != null)
							<p><b>Cidade: </b>{{ $evento->cidade }}</p>
						@endif
						@if($evento->estado != null)
							<p><b>Estado: </b>{{ $evento->estado }}</p>
						@endif
						@if($evento->descricao != null)
							<p><b>Descrição: </b><?php echo $evento->descricao ?></p>
						@endif
						<!--BOTÃO EDITAR-->
						<a href="{{ route('editar-evento',$evento->id) }}" class="btn btn-default btn-sm">Editar</a>

						<!--BOTÃO EXCLUIR-->
						{!! Form::open(['method' => 'DELETE','route' => ['deletar-evento',$evento->id],'style' => 'display:inline']) !!}
							<button class="btn btn-default btn-sm" onclick="return deletar({{ $evento->id }})">Excluir</button>
						{!! Form::close() !!}

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection