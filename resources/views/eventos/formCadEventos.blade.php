@extends('layouts.admin')

@section('page-wrapper')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-header">
					Eventos - <small>Cadastrar</small>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<!--MENSAGENS DE FEEDBACK-->
				@if(Session::has('msg-sucess'))
					<div class="alert alert-success">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('msg-sucess') }}
					</div>
				@endif
				@if(Session::has('msg-warning'))
					<div class="alert alert-warning">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('msg-warning') }}
					</div>
				@endif

				<!--FORMULÁRIO-->
				@if(isset($evento))
					{!! Form::model($evento,['method' => 'PATCH','route' => ['alterar-evento',$evento->id]]) !!}
				@else
					{!! Form::open(['route' => 'salvar-evento']) !!}
				@endif
					<!--campo nome e mensagens de validação-->
					<div class="form-group{{ $errors->has('nome') ? ' has-error':''}}">
						{!! Form::label('nome','Nome do evento') !!} <span class="signal">*</span>
						{!! Form::input('text', 'nome', null, ['class' => 'form-control','placeholder' => 'Informe o nome do evento']) !!}
						@if($errors->has('nome'))
							<span class="help-block">
								{{ $errors->first('nome') }}
							</span>
						@endif
					</div>
					<!--campo url do evento e mensagens de validação-->
					<div class="form-group">
						{!! Form::label('url','Link do evento') !!}
						{!! Form::input('text','url',null,['class' => 'form-control','placeholder' => 'Informe a url do evento']) !!}
					</div>
					<!--campo cidade do evento e mensagens de validação~-->
					<div class="form-group">
						{!! Form::label('cidade','Cidade do evento') !!}
						{!! Form::input('text','cidade',null,['class' => 'form-control','placeholder' => 'Informe a cidade de realização do evento']) !!}
					</div>
					<!--campo estado e mensagens de validação-->
					<div class="form-group">
						{!! Form::label('estado','Estado do evento') !!}
						{!! Form::select('estado',$estados,null,['class' => 'form-control','placeholder' => 'Informe o estado...']) !!}
					</div>
					<!--CAMPO DESCRIÇÃO E MENSAGENS DE VALIDAÇÃO-->
					<div class="form-group">
						{!! Form::label('descricao','Descrição') !!}
						{!! Form::textarea('descricao',null) !!}
					</div>
					<!--botão de salvar-->
					<div class="form-group">
						{!! Form::submit('Salvar',['class' => 'btn btn-primary']) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection