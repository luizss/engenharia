<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Notícias</title>

        <!-- Fonts -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/styleApp.css') }}">
        <!--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">-->

        <!-- Styles -->
        <style type="text/css">
            .img-logo{width: 80px;margin-top: -15px;}
            @media (min-width: 768px){
                .navbar-default .navbar-nav > li > a{padding-top: 28px; padding-bottom: 28px;}    
            }
            .navbar-default .navbar-toggle{
                margin-top: 22px;
                margin-bottom: 22px;
            }
        </style>
    </head>
    <body>

    <header>
        <nav class="navbar navbar-default navbar-expand-lg">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><img class="img-logo" src="{{ asset('img/logo-menor.jpg') }}"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="menu">
                    <ul class="nav navbar-nav navbar-right">
                        @if (Route::has('login'))
                            @if (Auth::check())
                                <li><a href="{{ url('/home') }}">Home</a></li>
                            @else
                                <li><a href="{{ url('/login') }}">Login</a></li>
                                <li><a href="{{ url('/register') }}">Cadastrar</a></li>
                            @endif
                        @endif
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h1 class="text-center">Notícias</h1>
                @if(count($news) > 0)
                    @foreach($news as $new)
                            <p><a href="noticias/{{ $new->id }}">{{ $new->titulo }}</a></p>
                    @endforeach
                @else
                    <h1><small>Não há notícias para exibir</small></h1>    
                @endif    
            </div>
            <div class="col-md-3">
                <h1 class="text-center">Egressos</h1>
                @if(sizeof($egressos) > 0)
                    @foreach($egressos as $egresso)
                        <p><a href="{{ route('egresso.detalhes',$egresso->id) }}">{{ $egresso->nome }}</a></p>
                    @endforeach
                @else
                    <h1><small>Não há egressos cadastrados</small></h1>    
                @endif
            </div>
            <div class="col-md-3">
                <h1 class="text-center">Pós-graduações</h1>
                @if(sizeof($pos) > 0)
                    @foreach($pos as $value)
                        <p><a href="">{{ $value->nome_pos }} - <small>{{ $value->instituicao }}</small></a></p>
                    @endforeach
                @else
                    <h1 class="text-center"><small>Não há pós-graduações para exibir</small></h1>
                @endif
            </div>
            <div class="col-md-3">
                <h1 class="text-center">Eventos</h1>
                @if(sizeof($eventos) > 0)
                    @foreach($eventos as $evento)
                        <p><a href="">{{ $evento->nome }}</a></p>
                    @endforeach
                @else
                    <div class="text-center"><small>Não há eventos cadastrados!</small></div>
                @endif
            </div>
        </div>
    </div>
    
    <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
