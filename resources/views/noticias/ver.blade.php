<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Notícias</title>

    <!-- Fonts -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styleApp.css') }}">
    <!--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">-->
</head>
<body>
<header>
    <nav class="navbar navbar-default navbar-expand-lg">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu"
                        aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">Engenharia de produção</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu">
                <ul class="nav navbar-nav navbar-right">
                    @if (Route::has('login'))
                        @if (Auth::check())
                            <li><a href="{{ url('/home') }}">Home</a></li>
                        @else
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Cadastrar</a></li>
                        @endif
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
</header>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>{{ $noticia->titulo }}</h2>
            
            <p>Autor: {{ $noticia->autor }}</p>
            
            <p>Atualizado em: {{ $noticia->updated_at->format('d/m/Y H:m:s') }}</p>
            {!! $noticia->noticia !!}  
        </div>
    </div>
    <!--verificando o tamanho do array imagens-->
    @if(sizeof($imagens) > 0)
        <div class="row">
            <div class="col-md-12">
                <h3>Confira as imagens</h3>
                @foreach($imagens as $imagem)
                    <img class="img img-responsive img-noticia" src="{{ asset('/storage/img_noticias/'.$imagem->image) }}" alt="Engenharia da producao - UEA">
                @endforeach
            </div>
        </div>    
    @endif
        
</div>

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>