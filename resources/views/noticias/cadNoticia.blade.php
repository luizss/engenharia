@extends('layouts.admin')

@section('page-wrapper')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{ $titlePage }} -
                    <small>{{ $subtitlePage }}</small>
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('msg-sucess'))
                    <div class="alert alert-success">
                        <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('msg-sucess') }}
                    </div>
                @endif
                @if(Session::has('msg-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true">&times;</button>
                        {{ Session::get('msg-warning') }}
                    </div>
                @endif
                
                <p><span class="signal">*</span> Campos obrigatórios</p>

                @if(Request::is('*/editar'))
                    {!! Form::model($news, ['method'=>'PATCH','url'=>'noticias/'.$news->id]) !!}
                @else
                    {!! Form::open(['url'=>'noticias/salvar']) !!}
                @endif
                
                <div class="form-group">
                {!! Form::label('titulo','Título') !!} <span class="signal">*</span>
                {!!
                Form::input('text','titulo',null,['class'=>'form-control
                form-group','auto-focus','placeholder'=>'Título da notícia'])
                !!}
                </div>

                <div class="form-group">
                {{ Form::label('noticia','Conteúdo da notícia') }} <span class="signal">*</span>
                {!!
                Form::textarea('noticia',null,['class'=>'form-control
                form-group','auto-focus','placeholder'=>'Conteúdo da notícia'])
                !!}
                </div>

                {{ Form::label('autor','Autor da notícia') }} <span class="signal">*</span>
                {!!
                Form::input('text','autor',null,['class'=>'form-control
                form-group','auto-focus','placeholder'=>'Conteúdo da notícia'])
                !!}

                <div class="form-group">
                    {!! Form::submit('Salvar',['class'=>'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
