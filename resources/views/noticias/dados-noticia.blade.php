@extends('layouts.admin')

@section('page-wrapper')
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-header">
				{{ $titlePage }} - <small>{{ $subtitlePage }}</small>
			</h1>
			<div class="col-sm-8">

				@if(Session::has('success'))
					<div class="alert alert-success">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('success') }}
					</div>
				@endif

				@if(Session::has('danger'))
					<div class="alert alert-danger">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('danger') }}
					</div>
				@endif

				<div class="panel panel-default">
					<div class="panel-body">
						<p><b>Título: </b>{{ $noticia->titulo }}</p>
						<p><b>Notícia: </b><?php echo $noticia->noticia; ?></p>
						<p><b>Autor: </b>{{ $noticia->autor }}</p>
						<!--Botão editar -->
						<a href="{{ route('editar-noticia',$noticia->id) }}" class="btn btn-default btn-sm">Editar</a>
						
						<!--botão excluir-->
						{!! Form::open(['method'=>'DELETE','route'=>['deletar-noticia',$noticia->id],'style'=>'display:inline']) !!}
							<button class="btn btn-default btn-sm" onclick="return deletar({{ $noticia->id }})">Deletar</button>
						{!! Form::close() !!}
						
						<!--Inserir image-->
						<a href="{{ route('noticia-nova-imagem',$noticia->id) }}" class="btn btn-default btn-sm">Inserir imagem
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection