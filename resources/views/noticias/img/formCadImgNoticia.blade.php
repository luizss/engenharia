@extends('layouts.admin')

@section('page-wrapper')
	<div class="container-fluid">
		<div class="row">
            <div class="col-xs-12">
				<h1 class="page-header">
					Noticía - <small>cadastro de imagem</small>
				</h1>
				@if(isset($errors) && count($errors) > 0)
					<div class="alert alert-warning">
						@foreach($errors->all() as $error)
							<p>{{ $error }}</p>
						@endforeach
					</div>
				@endif

				@if(Session::has('danger'))
					<div class="alert alert-danger">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('danger') }}
					</div>
				@endif

				{!! Form::open(['enctype' => 'multipart/form-data','route'=>'noticia-salvar-imagem']) !!}
					<div class="form-group">
						{!! Form::file('image',['class' => 'form-control']) !!}
					</div>
						{!! Form::hidden('noticia_id',$noticia->id) !!}
					<div class="form-group">
						{!! Form::submit('Salvar',['class' => 'btn btn-primary']) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection