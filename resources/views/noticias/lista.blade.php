@extends('layouts.admin')

@section('page-wrapper')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page-header">
                    Notícias
                </h1>
                @if(Session::has('msg-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true">&times;</button>
                        {{ Session::get('msg-warning') }}
                    </div>
                @endif
                @if(Session::has('msg-sucess'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true">&times;</button>
                        {{ Session::get('msg-sucess') }}
                    </div>
                @endif
                @if($nNews==0)
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true">&times;</button>
                        Nenhuma notícia cadastrada
                    </div>
                @else
                    <table class="table table-responsive tab-pane">
                        <th>Título</th>
                        <tbody>
                        @foreach($news as $new)
                            <tr>
                                <td><a href="{{ route('dados-noticia',$new->id) }}">{{ $new->titulo }}</a></td>
                            </tr>
                        @endforeach
                        </tbody> 
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection


