@extends('layouts.admin')

@section('page-wrapper')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{ $titlePage }} - <small>{{ $subtitlePage }}</small>
                </h1>
                @if(Session::has('msg-sucess'))
                    <div class="alert alert-success">
                        <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('msg-sucess') }}
                    </div>
                @endif
                @if(Session::has('msg-warning'))
                    <div class="alert alert-warning">
                        <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('msg-warning') }}
                    </div>
                @endif
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">

                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $news }}</div>
                                <div>Notícias</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ url('noticias/lista-noticias') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Ver lista</span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $egressos }}</div>
                                <div>Egressos</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('egressos-admin') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Ver lista</span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $pos }}</div>
                                <div>Pós-graduações</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('listapos') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Ver lista</span>
                            <div class="clearfix"></div>   
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $eventos }}</div>
                                <div class="clearfix">Eventos</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('lista-evento') }}">
                        <div class="panel-footer">
                            <div class="pull-left">Ver lista</div>
                            <div class="clearfix"></div>    
                        </div>
                    </a>
                </div>
            </div>

        </div>
        <!-- /.row -->
        <!-- /.row -->
        <!-- /.row -->
    </div>
@endsection
