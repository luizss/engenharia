@extends('layouts.admin')

@section('page-wrapper')
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-header">
					Pós-graduações - <small>Lista</small>
				</h1>
				@if(isset($pos))
				
					@if(Session::has('msg-warning'))
						<div class="alert alert-warning">
							<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
							{{ Session::get('msg-warning') }}
						</div>
					@endif

					<div class="table-responsive">
						<table class="table tb-pane">
							<tr>
								<th>Nome da pós</th>
								<th>Instituição</th>
							</tr>
							<tbody>
								@foreach($pos as $value)
									<tr>
										<td>
											<a href="{{ route('dados-pos',$value->id) }}">{{ $value->nome_pos }}</a>
										</td>
										<td>
											{{ $value->instituicao }}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@else
					<div class="alert alert-warning">Não há pós graduações cadastradas!</div>	
				@endif
			</div>
		</div>
	</div>
@endsection