@extends('layouts.admin')

@section('page-wrapper')
	<div class="container-fluid">
		<div class="row">
			<h1 class="page-header">
				Pós-graducações - <small>Dados</small>
			</h1>
			@if(Session::has('msg-sucess'))
				<div class="alert alert-success">
					<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('msg-sucess') }}
				</div>
			@endif
			@if(Session::has('msg-warning'))
				<div class="alert alert-warning">
					<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('msg-warning') }}
				</div>
			@endif

			<div class="col-sm-10">
				<div class="panel panel-default">
					<div class="panel-body">
						<p><b>Nome: </b>{{ $pos->nome_pos }}</p>
						<p><b>Instituição: </b>{{ $pos->instituicao }}</p>
						<p><b>Grau: </b>{{ $pos->grau }}</p>
						@if($pos->cidade != null)
							<p><b>Cidade: </b>{{ $pos->cidade }}</p>
						@endif
						@if($pos->estado != null)
							<p><b>Estado: </b>{{ $pos->estado }}</p>
						@endif
						<p><b>Url: </b>{{ $pos->url }}</p>
						@if($pos->data_fim_inscricao != null)
							<p><b>Término da inscricao: </b>{{ $data = substr($pos->data_fim_inscricao,8,2) ."/".substr($pos->data_fim_inscricao,5,2)."/".substr($pos->data_fim_inscricao, 0,4) }}</p>
						@endif
						<!--Botão editar -->
						<a class="btn btn-default btn-sm" href="{{ route('pos-editar',$pos->id) }}">Editar dados</a>	

						<!--botao excluir-->
						{!! Form::open(['method'=>'DELETE','route' => ['pos-deletar',$pos->id],'style' => 'display:inline']) !!}
							<button class="btn btn-default btn-sm" onclick="return deletar({{ $pos->id }})">Excluir</button>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection