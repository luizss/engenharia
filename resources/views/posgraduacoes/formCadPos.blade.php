@extends('layouts.admin')

@section('page-wrapper')
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-header">
					Pós-graduações - <small>Cadastro</small>
				</h1>
				<!--Mensagem falha-->
				@if(Session::has('msg-warning'))	
					<div class="alert alert-warning">
						{{ Session::get('msg-warning') }}
					</div>
				@endif
				<!--Mensagem sucesso -->
				@if(Session::has('msg-sucess'))
					<div class="alert alert-success">
						{{ Session::get('msg-sucess') }}
					</div>
				@endif
				
				<p><span class="signal">*</span> Campos obrigatórios</p>

				@if(isset($pos))
					{!! Form::model($pos,['method'=>'PATCH','route' => ['pos-alterar',$pos->id]]) !!}
				@else
					{!! Form::open(['route' => 'pos-salvar']) !!}
				@endif
					<!--Campo nome da pós-graduação-->
					<div class="form-group{{ $errors->has('nome_pos') ? ' has-error':'' }}">
						{!! Form::label('nome_pos','Nome da pós-graduação') !!} <span class="signal">*</span>
						{!! Form::input('text','nome_pos',null,['class' => 'form-control','placeholder' => 'Informe o nome da pós-graduação']) !!}
						@if($errors->has('nome_pos'))
							<span class="help-block">
								{{ $errors->first('nome_pos') }}
							</span>	
						@endif
					</div>
					<!--Campo nome instituicao com validacao-->
					<div class="form-group{{ $errors->has('instituicao') ? ' has-error': '' }}">
						{!! Form::label('instituicao','Instituição') !!} <span class="signal">*</span>
						{!! Form::input('text','instituicao',null,['class' => 'form-control','placeholder' => 'Nome da instituição']) !!}
						@if($errors->has('instituicao'))
							<span class="help-block">
								{{ $errors->first('instituicao') }}
							</span>
						@endif
					</div>
					<!--Campo grau da pós -->
					<div class="form-group{{ $errors->has('grau') ? ' has-error': '' }}">
						{!! Form::label('grau','Grau') !!} <span class="signal">*</span>
						{!! Form::select('grau', $grau, null,['class' => 'form-control','placeholder' => 'Informe o grau da pós-graduação...']) !!}
						@if($errors->has('grau'))
							<span class="help-block">
								{{ $errors->first('grau') }}
							</span>
						@endif
					</div>
					<!--Campo cidade da pós-->
					<div class="form-group">
						{!! Form::label('cidade','Cidade da pós') !!}
						{!! Form::input('text','cidade',null,['class' => 'form-control', 'placeholder' => 'Informe a cidade, se houver']) !!}
					</div>
					<!--CAMPO ESTADO DA PÓS-->
					<div class="form-group">
						{!! Form::label('estado','Estado onde será realizada') !!}
						{!! Form::select('estado',$estados,null,['class' => 'form-control','placeholder' => 'Informe o estado, se houver...']) !!}
					</div>
					<!--CAMPO URL DA PÓS-->
					<div class="form-group{{ $errors->has('url') ? ' has-error': '' }}">
						{!! Form::label('url','Link para mais informações ou inscrição') !!} <span class="signal">*</span>
						{!! Form::input('text','url',null,['class' => 'form-control', 'placeholder' => 'Informe a url']) !!}
						@if($errors->has('url'))
							<span class="help-block">
								{{ $errors->first('url') }}
							</span>
						@endif
					</div>
					<!--CAMPO DATA FIM INSCRIÇÃO DA PÓS-->
					<div class="form-group{{ $errors->has('data_fim_inscricao') ? ' has-error': '' }}">
						{!! Form::label('data_fim_inscricao','Data do término das inscrições') !!}
						{!! Form::date('data_fim_inscricao',null,['class' => 'form-control']) !!}
						@if($errors->has('data_fim_inscricao'))
							<span class="help-block">
								{{ $errors->first('data_fim_inscricao') }}
							</span>
						@endif
					</div>	
					<div class="form-group">
						{!! Form::submit('Salvar',['class' => 'btn btn-primary']) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection