@extends('layouts.admin');
@section('page-wrapper')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{ $titlePage }} -
                    <small>{{ $subtitlePage }}</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('msg-sucess'))
                    <div class="alert alert-success">
                        <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('msg-sucess')}}
                    </div>
                @endif
                @if(Session::has('msg-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('msg-warning') }}
                    </div>
                @endif
                <!--Início do formulário-->
                @if(isset($egresso))
                <!--recuperando dados no caso de edição de dados-->
                    {!! Form::model($egresso,['method'=>'PATCH','route' => ['egresso-atualizar',$egresso->id]]) !!}
                @else
                <!--Iniciando formulário no caso de cadastro de informações-->
                    {!! Form::open(['route' => 'salvar-egresso']) !!}
                @endif
                <!--Campo nome com a mensagens de validação-->
                <div class="form-group{{ $errors->has('nome') ? ' has-error':'' }}">
                    {!! Form::label('nome','Nome') !!}
                    {!! Form::input('text','nome',null,['class'=>'form-control', 'placeholder' => 'Informe o nome']) !!}
                    @if ($errors->has('nome'))
                        <span class="help-block">
                             {{ $errors->first('nome') }}
                        </span>
                    @endif
                </div>

                <!--Campo ano com mensagens de validação-->
                <div class="form-group{{ $errors->has('ano') ? ' has-error':'' }}">
                    {{ Form::label('ano','Ano de formação') }}
                    <select class="form-control" id="ano" name="ano">
                        <option value="">Escolha o ano</option>
                        @for($i = date('Y');$i >= 2005; $i--)
                            <option value="{{ $i }}"
                            @if(isset($egresso->ano) && $egresso->ano == $i)
                                selected
                            @endif
                            >{{ $i or old('ano')}}</option>
                        @endfor
                    </select>
                    @if($errors->has('ano'))
                        <span class="help-block">
                            {{ $errors->first('ano') }}
                        </span>
                    @endif
                </div>

                <!--campo empresa com mensagens de validação de dados-->
                <div class="form-group{{ $errors->has('empresa') ? ' has-error':'' }}">
                    {{ Form::label('empresa','Empresa atual') }}
                    {{ Form::input('text','empresa',null,['class' => 'form-control','placeholder' => 'Nome da empresa']) }}
                    @if($errors->has('empresa'))
                        <span class="help-block">
                            {{ $errors->first('empresa') }}
                        </span>
                    @endif
                </div>

                <!--campo cargo com mensagens de validação de dados-->
                <div class="form-group{{ $errors->has('cargo') ? ' has-error':'' }}">
                    {{ Form::label('cargo','Cargo na empresa atual') }}
                    {{ Form::input('text','cargo',null,['class' => 'form-control','placeholder' => 'Cargo que ocupa']) }}
                    @if($errors->has('cargo'))
                        <span class="help-block">
                            {{ $errors->first('cargo') }}
                        </span>
                    @endif
                </div>

                <!--campo resumo da função com mensagens de validação de dados definidas na Model Egresso.php-->
                <div class="form-group{{ $errors->has('funcao') ? ' has-error':'' }}">
                    {{ Form::label('funcao','Resumo de sua função') }}
                    {{ Form::textarea('funcao',null) }}
                    @if($errors->has('funcao'))
                        <span class="help-block">{{ $errors->first('funcao') }}</span>
                    @endif
                </div>

                <!--Botão para salvar as informações inseridas no formulário-->
                <div class="form-group">
                    {{Form::submit('Salvar',['class' => 'btn btn-primary'])}}
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection