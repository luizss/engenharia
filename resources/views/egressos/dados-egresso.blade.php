@extends('layouts.admin')

@section('page-wrapper')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				{{ $titlePage }} - <small>{{ $subtitlePage }}</small>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			@if(Session::has('msg-success'))
				<div class="alert alert-success">
					<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('msg-success') }}
				</div>
			@endif
			@if(Session::has('msg-warning'))
				<div class="alert alert-warning">
					<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('msg-warning') }}
				</div>
			@endif

			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-body">
						@if(isset($egresso->imagem->image))
						<img class="img img-responsive img-egresso" src="{{ asset('/storage/img_egresso/'.$egresso->imagem->image) }}" alt="{{ $egresso->imagem->image }}">
						@endif
						<p><b>Nome: </b>{{ $egresso->nome }}</p>
						<p><b>Ano de formação: </b>{{ $egresso->ano }}</p>
						<p><b>Empresa atual: </b>{{ $egresso->empresa }}</p>
						<p><b>Cargo: </b>{{ $egresso->cargo }}</p>
						<p><b>Resumo da função: </b><?php echo $egresso->funcao; ?></p>
						<!-- botões para as ações -->
						<!-- editar-->
						<a class="btn btn-default btn-sm" href="{{ route('editar-egresso',$egresso->id) }}">Editar</a>
						<!-- Deletar -->
						{!! Form::open(['method'=>'DELETE','route'=>['egresso-deletar',$egresso->id],'style'=>'display:inline']) !!}
							<button class="btn btn-default btn-sm" onclick="return deletar({{ $egresso->id }})">Excluir</button>
						{!! Form::close() !!}

						<!-- Adicionar imagem -->
						<a class="btn btn-default btn-sm" href="{{ route('egresso-nova-imagem',$egresso->id) }}">Cadastrar imagem</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection