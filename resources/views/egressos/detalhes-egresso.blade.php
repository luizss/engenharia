@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				@if(isset($egresso))
					<div class="row">
						<div class="col-md-12">
							<img class="img img-responsive img-egresso" src="{{ asset('/storage/img_egresso/'	.$egresso->imagem->image) }}">	
						</div>	
					</div>	
					<div c	lass="row">	
						<d	iv class="col-md-12">	
								<h1>Dados do egress	o</h1>	
								<p><b>Nome: </b>{{ 	$egresso->nome }}</p>	
								<p><b>Ano de formaç	ão: </b>{{ $egresso->ano }}</p>	
								<p><b>Empresa atual	: </b>{{ $egresso->empresa }}</p>	
								<p><b>Cargo que ocu	pa: </b>{{ $egresso->cargo }}</p>	
							<p><b>Função: </b><?php echo $egresso->funcao; ?></p>	
						</div>
					</div>	
				@else
					<div class="alert alert-warning">
						Registro não encontrado!
					</div>	
				@endif
			</div>	
		</div>
	</div>
@endsection