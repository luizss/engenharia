@extends('layouts.admin')

@section('page-wrapper')
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-header">
				{{ $titlePage }} - <small>{{ $subtitlePage }}</small> 
			</h1>
			@if(isset($errors) && count($errors) > 0)
				<div class="alert alert-danger">
					@foreach($errors->all() as $error)
						<p>{{ $error }}</p>
					@endforeach
				</div>
			@endif

			@if(Session::has('msg-error'))
			 	<div class="alert alert-danger">
			 		<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
			 		{{ Session::get('msg-error') }}
			 	</div>
			@endif

			<form method="post" action="{{ url('egressos/image/salvar') }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="image">Imagem</label>
					<input type="file" name="image" class="form-control">
				</div>
				
				<input type="hidden" name="egresso_id" value="{{ $egresso->id }}">

				<div class="form-group">
					<input type="submit" value="Salvar">
				</div>
			</form>	
		</div>
	</div>	
</div>
@endsection
