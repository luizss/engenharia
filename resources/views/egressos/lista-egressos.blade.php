@extends('layouts.admin')

@section('page-wrapper')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
            <h1 class="page-header">
                Egressos - <small>Lista</small>
            </h1>
        @if(Session::has('msg-sucess'))
            <div class="alert alert-success">
                <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('msg-sucess') }}
            </div>
        @endif
        
        @if(Session::has('msg-warning'))
            <div class="alert alert-warning">
                <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('msg-warning') }}
            </div>
        @endif
        @if ($nEgressos == 0)
            <div class="alert alert-warning">
                Nenhum egresso cadastrado!
            </div>
        @else
            <div class="table-responsive">
                <table class="table tab-pane">
                    <tr>
                        <th>Nome</th>
                        <th>Ano de formação</th>
                        <th>Empresa atual</th>
                        <th>Cargo</th>
                    </tr>
                    <tbody>
                    @foreach ($egressos as $egresso)
                        <tr>
                            <td><a href="{{ route('dados-egresso',$egresso->id) }}">{{ $egresso->nome }}</a></td>
                            <td>{{ $egresso->ano }}</td>
                            <td>{{ $egresso->empresa }}</td>
                            <td>{{ $egresso->cargo }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        @endif

            </div>
        </div>
    </div>
@endsection