<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Egresso extends Model
{
    protected $fillable = ['nome','ano','empresa','cargo','funcao'];

    //relacionamento one to one entre egresso e imagem
    public function imagem(){
      return $this->hasOne(ImageEgresso::class);
    }

    public $rules = [
      'nome' => 'required|min:8|max:70',
      'ano' => 'required',
      'empresa' => 'required|min:2|max:50',
      'cargo' => 'required|min:5|max:50',
      'funcao' => 'required|min:20'
    ];

    public $messages = [
        'nome.required' => 'O campo nome deve ser preenchido',
        'nome.min' => 'O campo nome deve ter entre 8 e 70 caracteres',
        'nome.max' => 'O campo nome deve ter entre 8 e 70 caracteres',
        'ano.required' => 'O campo ano deve ser preenchido',
        'empresa.required' => 'O campo empresa deve ser preenchido',
        'empresa.min' => 'O campo empresa deve ter entre 2 e 50 caracteres',
        'empresa.max' => 'O campo empresa deve ter entre 2 e 50 caracteres',
        'cargo.required' => 'O campo cargo deve ser preenchido',
        'cargo.min' => 'O campo cargo deve ter entre 5 e 50 caracteres',
        'cargo.max' => 'O campo cargo deve ter entre 5 e 50 caracteres',
        'funcao.required' => 'O resumo da função deve ser preenchido',
        'funcao.min' => 'O resumo deve conter no mínimo 20 caracteres'

    ];
}
