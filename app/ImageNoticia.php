<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageNoticia extends Model
{
    protected $fillable = ['image','noticia_id'];

    public function noticia(){
    	return $this->belongsTo(Noticia::class);
    }

    public $rules = [
    	'image' => 'required'
    ];

    public $messages = [
    	'image.required' => 'Escolha a imagem'
    ];
}
