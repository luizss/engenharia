<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
/*use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
*/
use App\ImageEgresso;
use App\Egresso;
class ImageEgressosController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }

   	public function newImage($id){
   		//Seleciona o usuário com o id especificado
   		$nEgresso = Egresso::where('id',$id)->get()->count();
   		//Se o número de egressos (nEgressos) for maior que zero ...
   		if($nEgresso > 0){
   			//Recupera os dados do egresso
   			$egresso = Egresso::find($id);
   			//chama a view e manda os dados de titulo e subtitulo da página e os dados do egresso
   			$titlePage = 'Egresso';
            $subtitlePage = 'Imagem';
   			return view('egressos.img.formCadImg',compact('titlePage','subtitlePage','egresso')); 
   		}else{
            //se o número de egressos recuperados for igual a 0, redireciona para a lista de egressos com uma mensagem de erro
            Session::flash('msg-warning','Registro não encontrado');
            return redirect()->route('egressos-admin');
         }
   	}

      public function salvar(Request $request){
         //cria um objeto para validação
         $valida = new ImageEgresso();
         //realiza a validação dos campos
         $this->validate($request,$valida->rules,$valida->messages);
         //define um valor padrão para o nome do arquivo
         $nameFile = null;
         //recupera informações do egresso para usar na construcao do nome do arquivo
         $egresso = Egresso::find($request->egresso_id);
         //substitui os espaços em branco no nome do egresso por '_'
         $egresso->nome = str_replace(" ", "_", $egresso->nome);
         //monta o nome do arquivo juntando o nome com o id
         $img_name = $egresso->nome."_".$egresso->id;
         //recupera a extensao do arquivo enviado pelo formulário
         $extension = $request->image->extension();
         //monta o nome do arquivo com o nome.extensao
         $nameFile = $img_name.".".$extension;
         //faz o upload do arquivo
         $upload = $request->image->storeAs('img_egresso',$nameFile);

         if($upload){
            $data = [
               'image' => $nameFile, 
               'egresso_id' => $request->egresso_id
            ];
            $salvar = ImageEgresso::create($data);

            if ($salvar) {
               Session::flash('msg-success','Imagem salva com sucesso!');
               return redirect()->route('dados-egresso',$egresso->id);
            } else {
               Session::flash('msg-error','A imagem não pode ser cadastrada');
               return redirect()->route('egresso-nova-imagem',$egresso->id);
            }
            
         }else{
            Session::flash('msg-error','Upload não realizado!');
            return redirect()->route('egresso-nova-imagem',$egresso->id)->withInput();
         }
      }
}
