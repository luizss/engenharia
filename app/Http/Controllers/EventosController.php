<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Evento;
class EventosController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }
    public function novo(){
        $estados = ['AC' => 'Acre','AL' => 'Alagoas','AP' => 'Amapá','AM' => 'Amazonas','BA' => 'Bahia','CE' => 'Ceará','DF' => 'Distrito Federal','ES' => 'Espírito Santo','GO' => 'Goiás','MA' => 'Maranhão','MG'=> 'Minas Gerais','MS' => 'Mato Grosso do Sul','MT' => 'Mato Grosso','PA' => 'Pará','PB' => 'Paraíba','PE' => 'Pernambuco','PI' => 'Piauí','PR' => 'Paraná','RJ' => 'Rio de Janeiro','RN' => 'Rio Grande do Norte','RO' => 'Rondônia','RR' => 'Roraima','RS' => 'Rio Grande do Sul','SC' => 'Santa Catarina','SE' => 'Sergipe','SP' => 'São Paulo','TO' => 'Tocantins'];
    	return view('eventos.formCadEventos',compact('estados'));
    }

    public function salvar(Request $request){
    	$valida = new Evento();
    	$this->validate($request,$valida->rules,$valida->messages);
    	$eventos = Evento::where('nome',$request->nome)->get()->count();
    	if($eventos > 0){
            Session::flash('msg-warning','Já existe um evento com este nome');
            return redirect()->route('cadastro-evento')->withInput();
        }else{
            $data = $request->all();
            $evento = Evento::create($data);
            if($evento){
                Session::flash('msg-sucess','Evento cadastrado com sucesso');
                return redirect()->route('home');
            }else{
                Session::flash('msg-warning','Cadastro não realizado!');
                return redirect()->route('cadastro-evento')->withInput();
            }
    	}    
    }

    public function lista(){
        $eventos = Evento::orderBy('id','desc')->get();
        return view('eventos.lista-eventos',compact('eventos'));
    }

    public function dados($id){
        $evento = Evento::find($id);
        if(sizeof($evento) > 0){
            return view('eventos.dados-evento',compact('evento'));
        }else{
            Session::flash('msg-warning','Dados não encontrados!');
            return redirect()->route('lista-evento');
        }
    }

    public function editar($id){
        $evento = Evento::find($id);
        if(count($evento) > 0){
            $estados = ['AC' => 'Acre','AL' => 'Alagoas','AP' => 'Amapá','AM' => 'Amazonas','BA' => 'Bahia','CE' => 'Ceará','DF' => 'Distrito Federal','ES' => 'Espírito Santo','GO' => 'Goiás','MA' => 'Maranhão','MG'=> 'Minas Gerais','MS' => 'Mato Grosso do Sul','MT' => 'Mato Grosso','PA' => 'Pará','PB' => 'Paraíba','PE' => 'Pernambuco','PI' => 'Piauí','PR' => 'Paraná','RJ' => 'Rio de Janeiro','RN' => 'Rio Grande do Norte','RO' => 'Rondônia','RR' => 'Roraima','RS' => 'Rio Grande do Sul','SC' => 'Santa Catarina','SE' => 'Sergipe','SP' => 'São Paulo','TO' => 'Tocantins'];
            return view('eventos.formCadEventos',compact('estados','evento'));
        }else{
            Session::flash('msg-warning','Evento não encontrado!');
            return redirect()->route('lista-evento');
        }
    }

    public function alterar($id, Request $request){
        $valida = new Evento();
        $this->validate($request,$valida->rules,$valida->messages);
        $data = $request->all();
        $evento = Evento::find($id);
        if($evento->update($data)){
            Session::flash('msg-sucess','Alteração realizada com sucesso!');
            return redirect()->route('dados-evento',$evento->id);
        }else{
            Session::flash('msg-warning','O registro não pode ser alterado!');
            return redirect()->route('dados-evento',$evento->id);
        }
    }

    public function excluir($id){
        $evento = Evento::find($id);
        if($evento->delete()){
            Session::flash('msg-sucess','Exclusão realizada com sucesso!!');
            return redirect()->route('lista-evento');
        }
    }
}
