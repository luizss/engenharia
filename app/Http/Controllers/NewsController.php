<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\User;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function cadNoticia()
    {
        $titlePage = 'Notícias';
        $subtitlePage = 'Cadastro';
        return view('noticias.cadNoticia',compact('titlePage','subtitlePage'));
    }

    private function validaNoticia($request)
    {
        $validator = Validator::make($request->all(), [
            'titulo' => ['required', 'min:10', 'max:255'],
            'noticia' => ['required']
        ]);
        return $validator;
    }

    public function store(Request $request)
    {
        $validator = $this->validaNoticia($request);
        $nNews = News::where('titulo', $request->titulo)->get()->count();

        if ($validator->fails()) {
            Session::flash('msg-warning', 'Alguns dados estão incorretos. Por favor, verifique');
            return Redirect::to('nova-noticia');
        } else if ($nNews > 0) {
            Session::flash('msg-warning', 'Notícia já cadastrada!');
            return Redirect::to('nova-noticia');
        } else {
            $data = $request->all();

            $news = News::create($data);
            if ($news) {
                Session::flash('msg-sucess', 'Cadastro realizado com sucesso!');
                return redirect()->route('home');
            }
        }
    }

    public function editar($id)
    {
        $nNews = News::where('id', $id)->get()->count();
        if ($nNews > 0) {
            $titlePage = 'Notícias';
            $subtitlePage = 'Edição';
            $news = News::find($id);
            return view('noticias.cadNoticia', compact('news','titlePage','subtitlePage'));
        } else {
            Session::flash('msg-warning','Notícia não encontrada');
            return Redirect::back();
        }
    }

    public function atualizar($id, Request $request)
    {
        $news = News::find($id);
        $news->update($request->all());
        Session::flash('msg-sucess', 'Edição realizada com sucesso!');
        return Redirect::to('noticias/' . $news->id . '/editar');
    }

    public function delete($id)
    {
        $news = News::findOrFail($id);

        $news->delete();
        Session::flash('msg-sucess', 'Registro excluído com sucesso');
        return Redirect::to('noticias/lista-noticias/' . $news->user_id);
    }

    public function lista($id)
    {
        $user = User::find($id);
        $news = News::where('user_id', $user->id)->get();
        /*
         * Comando abaixo faz a contagem das linhas recuperadas com as notícias relacionadas ao usuario determinado
         */
        $nNews = News::where('user_id', $user->id)->get()->count();
        return view('noticias.lista', ['news' => $news, 'nNews' => $nNews]);
    }
}
