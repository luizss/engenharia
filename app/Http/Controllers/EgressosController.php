<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Egresso;

class EgressosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $egressos = Egresso::orderBy('ano', 'desc')->get();
        $nEgressos = Egresso::get()->count();
        return view('egressos/lista-egressos', compact('egressos', 'nEgressos'));
    }

    public function novo()
    {
        $titlePage = 'Egressos';
        $subtitlePage = 'Cadastrar';
        return view('egressos.formCadEgresso',compact('titlePage','subtitlePage'));
    }

    public function salvar(Request $request)
    {
        $valida = new Egresso();
        $this->validate($request,$valida->rules,$valida->messages);
        $nEgresso = Egresso::where('nome',$request->nome)->where('ano',$request->ano)->get()->count();
        if($nEgresso > 0){
            Session::flash('msg-warning','Já existe um registro com os mesmos dados');
            return redirect()->route('novo-egresso');
        }else{
            $data = $request->all();
            $salvar = Egresso::create($data);
            if($salvar){
                Session::flash('msg-sucess','Cadastro realizado com sucesso');
                return redirect()->route('home');
            }
        }

    }

    public function editar($id)
    {
        $nEgresso = Egresso::where('id',$id)->get()->count();
        if ($nEgresso > 0) 
        {
            $titlePage = 'Egresso';
            $subtitlePage = 'editar';
            $egresso = Egresso::find($id);
            return view('egressos.formCadEgresso',compact('egresso','titlePage','subtitlePage'));
        }else{
            Session::flash('msg-warning','Registro não encontrado');
            return redirect()->route('home');
        }
    }

    public function alterar($id, Request $request)
    {
        $valida = new Egresso();
        $this->validate($request, $valida->rules, $valida->messages);
        $dataForm = $request->all();
        $egresso = Egresso::find($id);
        if($egresso->update($dataForm)){
            Session::flash('msg-sucess','Registro atualizado com sucesso!!');
            return redirect()->route('editar-egresso',$egresso->id);
        }else{
            Session::flash('msg-warning','Falha ao atualizar registro!');
            return redirect()->route('editar-egresso',$egresso->id);
        }

    }

    public function deletar($id)
    {
        $egresso = Egresso::find($id);
        if($egresso->delete()){
            Session::flash('msg-sucess','Registro Excluído com sucesso!');
            return redirect()->route('egressos-admin');
        }else{
            Session::flash('msg-warning','Falha ao excluir registro');
            return redirect()->route('dados-egresso',$egresso->id);
        }
    }
    public function dados($id){
        $egresso = Egresso::where('id',$id)->with('imagem')->get()->count();
        if ($egresso > 0) {
            $egresso = Egresso::find($id);
            $titlePage = 'Egresso';
            $subtitlePage = 'Dados';
            return view('egressos.dados-egresso',compact('egresso','titlePage','subtitlePage'));
        } else {
            Session::flash('msg-warning','Registro não encontrado!');
            return redirect()->route('home');
        }
        
    }
}
