<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\PosGraduacao;

class PosGraduacoesController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }
    public function index(){
    	$nPos = PosGraduacao::count();
    	if($nPos > 0){
    		$pos = PosGraduacao::orderBy('id','desc')->get();
    		return view('posgraduacoes.lista-pos',compact('pos'));
    	}else{
    		return view('posgraduacoes.lista-pos');
    	}
    }

    public function novo(){
        $estados = ['AC' => 'Acre','AL' => 'Alagoas','AP' => 'Amapá','AM' => 'Amazonas','BA' => 'Bahia','CE' => 'Ceará','DF' => 'Distrito Federal','ES' => 'Espírito Santo','GO' => 'Goiás','MA' => 'Maranhão','MG'=> 'Minas Gerais','MS' => 'Mato Grosso do Sul','MT' => 'Mato Grosso','PA' => 'Pará','PB' => 'Paraíba','PE' => 'Pernambuco','PI' => 'Piauí','PR' => 'Paraná','RJ' => 'Rio de Janeiro','RN' => 'Rio Grande do Norte','RO' => 'Rondônia','RR' => 'Roraima','RS' => 'Rio Grande do Sul','SC' => 'Santa Catarina','SE' => 'Sergipe','SP' => 'São Paulo','TO' => 'Tocantins'];
        $grau = ['Pós-gradução' => 'Pós-gradução','Mestrado' => 'Mestrado','Doutorado' => 'Doutorado'];
        return view('posgraduacoes.formCadPos',compact('grau','estados'));
    }

    public function salvar(Request $request){
        $valida = new PosGraduacao();
        $this->validate($request,$valida->rules,$valida->messages);
        $nPos = PosGraduacao::where('nome_pos',$request->nome_pos)->where('instituicao',$request->instituicao)->where('url',$request->url)->get()->count();
        if($nPos > 0){
            Session::flash('msg-warning','Já existe um registro com esses dados');
            return redirect()->route('poscadastro')->withInput();
        }else{
            $data = $request->all();

            $salvar = PosGraduacao::create($data);
            if($salvar){
                Session::flash('msg-sucess','Cadastro realizado!');
                return redirect()->route('poscadastro');
            }
        }
    }
    public function editar($id){
        $nPos = PosGraduacao::where('id',$id)->get()->count();
        if($nPos > 0){
            $pos = PosGraduacao::find($id);
            $estados = ['AC' => 'Acre','AL' => 'Alagoas','AP' => 'Amapá','AM' => 'Amazonas','BA' => 'Bahia','CE' => 'Ceará','DF' => 'Distrito Federal','ES' => 'Espírito Santo','GO' => 'Goiás','MA' => 'Maranhão','MG'=> 'Minas Gerais','MS' => 'Mato Grosso do Sul','MT' => 'Mato Grosso','PA' => 'Pará','PB' => 'Paraíba','PE' => 'Pernambuco','PI' => 'Piauí','PR' => 'Paraná','RJ' => 'Rio de Janeiro','RN' => 'Rio Grande do Norte','RO' => 'Rondônia','RR' => 'Roraima','RS' => 'Rio Grande do Sul','SC' => 'Santa Catarina','SE' => 'Sergipe','SP' => 'São Paulo','TO' => 'Tocantins'];
            $grau = ['Pós-gradução' => 'Pós-gradução','Mestrado' => 'Mestrado','Doutorado' => 'Doutorado'];
            return view('posgraduacoes.formCadPos',compact('pos','grau','estados'));
        }else{
            Session::flash('msg-warning','Dados não encontrados!');
            return redirect()->route('listapos');
        }
    }
    public function dados($id){
        $nPos = PosGraduacao::where('id',$id)->get()->count();
        if($nPos > 0){
            $pos = PosGraduacao::find($id);
            return view('posgraduacoes.dados-pos',compact('pos'));    
        }else{
            Session::flash('msg-warning','Dados não encontrados');
            return redirect()->route('listapos');
        }
    }

    public function alterar($id, Request $request){
        $valida = new PosGraduacao();
        $this->validate($request,$valida->rules,$valida->messages);
        $data = $request->all();
        $pos = PosGraduacao::find($id);
        if($pos->update($data)){
            Session::flash('msg-sucess','Alteração realizada');
            return redirect()->route('dados-pos',$pos->id);
        }else{
            Session::flash('msg-warning','Não foi possível fazer a alteração');
            return redirect()->route('dados-pos',$pos->id);
        }
    }

    public function deletar($id){
        $pos = PosGraduacao::find($id);
        if($pos->delete()){
            Session::flash('msg-sucess','Registro excluído!');
            return redirect()->route('home');
        }
    }
}
