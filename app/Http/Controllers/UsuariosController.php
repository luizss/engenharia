<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use App\User;
use App\Egresso;
use App\PosGraduacao;
use App\Evento;
class UsuariosController extends Controller
{
    public function index(){
        $news = Noticia::orderBy('id','desc')->limit(5)->get();
        $pos = PosGraduacao::orderBy('id','desc')->get();
        $egressos = Egresso::orderBy('nome')->get();
        $eventos = Evento::orderBy('id')->get();
        return view('welcome',compact('news','pos','egressos','eventos'));
    }

    public function visualizar($id){
        $noticia = Noticia::find($id);
        $imagens = $noticia->images()->get();
        return view('noticias.ver',compact('noticia','imagens'));
    }
    public function egressos(){
        $egressos = Egresso::orderBy('ano','desc')->get();
        $nEgressos = Egresso::get()->count();
        return view('egressos/lista-egressos',compact('egressos','nEgressos'));
    }
    public function viewEgresso($id){
        $egresso = Egresso::find($id);
        if($egresso){
            $imagem = $egresso->imagem;
            return view('egressos.detalhes-egresso',compact('egresso','imagem'));
        }else{
            return view('egressos.detalhes-egresso');
        }
    }
}
