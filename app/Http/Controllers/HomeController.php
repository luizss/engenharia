<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use App\Egresso;
use App\PosGraduacao;
use App\Evento;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Administrador';
        $subtitlePage = 'Dados';
        $news = Noticia::count();
        $egressos = Egresso::count();
        $pos = PosGraduacao::count();
        $eventos = Evento::count();
        return view('home',compact('titlePage','subtitlePage','news','egressos','pos','eventos'));
    }
}
