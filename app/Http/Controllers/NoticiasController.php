<?php

namespace App\Http\Controllers;

use App\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class NoticiasController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function cadNoticia()
    {
        $titlePage = 'Notícias';
        $subtitlePage = 'Cadastro';
        return view('noticias.cadNoticia',compact('titlePage','subtitlePage'));
    }

    private function validaNoticia($request)
    {
        $validator = Validator::make($request->all(), [
            'titulo' => ['required', 'min:10', 'max:255'],
            'noticia' => ['required'],
            'autor' => ['required','min:6','max:60']
        ]);
        return $validator;
    }

    public function store(Request $request)
    {
        $validator = $this->validaNoticia($request);
        $nNews = Noticia::where('titulo', $request->titulo)->get()->count();

        if ($validator->fails()) {
            Session::flash('msg-warning', 'Alguns dados estão incorretos. Por favor, verifique');
            return Redirect::to('nova-noticia');
        } else if ($nNews > 0) {
            Session::flash('msg-warning', 'Notícia já cadastrada!');
            return Redirect::to('nova-noticia');
        } else {
            $data = $request->all();

            $news = Noticia::create($data);
            if ($news) {
                Session::flash('msg-sucess', 'Cadastro realizado com sucesso!');
                return redirect()->route('home');
            }
        }
    }

    public function editar($id)
    {
        $nNews = Noticia::where('id', $id)->get()->count();
        if ($nNews > 0) {
            $titlePage = 'Notícias';
            $subtitlePage = 'Edição';
            $news = Noticia::find($id);
            return view('noticias.cadNoticia', compact('news','titlePage','subtitlePage'));
        } else {
            Session::flash('msg-warning','Notícia não encontrada');
            return Redirect::back();
        }
    }

    public function atualizar($id, Request $request)
    {
        $news = Noticia::find($id);
        $news->update($request->all());
        Session::flash('msg-sucess', 'Edição realizada com sucesso!');
        return Redirect::to('noticias/' . $news->id . '/editar');
    }

    public function delete($id)
    {
        $news = Noticia::findOrFail($id);

        $news->delete();
        Session::flash('msg-sucess', 'Registro excluído com sucesso');
        return Redirect::to('noticias/lista-noticias');
    }

    public function lista()
    {
        /*
         * Comando abaixo faz a contagem das linhas recuperadas com as notícias relacionadas ao usuario determinado
         */
        $nNews = Noticia::get()->count();
        $news = Noticia::orderBy('id','desc')->get();
        return view('noticias.lista', ['news' => $news, 'nNews' => $nNews]);
    }
    public function dadosNoticia($id){
        $nNoticia = Noticia::where('id',$id)->count();

        if($nNoticia > 0){
            $noticia = Noticia::find($id);
            $titlePage = "Notícia";
            $subtitlePage = "Dados";

            return view('noticias.dados-noticia',compact('noticia','titlePage','subtitlePage'));
        }else{
            Session::flash('msg-warning','Registro não encontrado!');
            return redirect('noticias/lista-noticias');
        }
    }
}
