<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ImageNoticia;
use App\Noticia;

class ImageNoticiasController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }
    public function nova($id){
    	$nNoticias = Noticia::where('id',$id)->count();

    	if($nNoticias > 0){
    		$noticia = Noticia::find($id);
	    	return view('noticias.img.formCadImgNoticia',compact('noticia'));
    	}else{
            Session::flash('msg-warning','Notícia inválida!');
    		return redirect()->route('lista-noticias');
    	}
    }

    public function salvar(Request $request){
            $valida = new ImageNoticia();
            $this->validate($request,$valida->rules,$valida->messages);

            $extension = $request->image->extension();
            if($extension!="jpeg" && $extension!="png"){
                Session::flash('danger','Formato de imagem inválido');
                return redirect()->back();
            }else{
                $fileName = null;
                $name = uniqid(date('HisYmd'));
                $fileName = $name.".".$extension;
                
                $upload = $request->image->storeAs('img_noticias',$fileName);
                if($upload){
                    $data = ['image' => $fileName, 'noticia_id' => $request->noticia_id];
                    $salvar = ImageNoticia::create($data);
                    if($salvar){
                        Session::flash('success','Imagem cadastrada com sucesso!');
                        return redirect()->route('dados-noticia',$request->noticia_id);
                    }else{
                        Session::flash('danger','Ocorreu um erro e a imagem não pode ser salva!');
                        return redirect()->route('dados-noticia',$request->noticia_id);
                    }
                }else{
                    Session::flash('danger','O upload da imagem não pode ser feito');
                    return redirect()->route('noticia-nova-imagem',$request->noticia_id);
                }
            }   
        }
}
