<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable = ['nome','url','cidade','estado','descricao'];

    public $rules = [
    	'nome' => 'required|min:10|max:255'
    ];

    public $messages = [
    	'nome.required' => 'O nome é de preenchimento obrigatório',
    	'nome.min' 		=> 'O nome deve conter entre 10 e 255 caracteres',
    	'nome.max' 		=> 'O nome deve conter entre 10 e 255 caracteres'
    ];
}
