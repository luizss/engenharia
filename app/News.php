<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\User;

class News extends Model
{
    protected $fillable = ['titulo', 'noticia', 'user_id'];

    public function user()
    {
        //return $this->belongsTo(User::class);
        $query = $this->belongsTo(User::class,'user_id');
        if($query)
            $query = $query->select('id','name');
        return $query;
    }
}
