<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $fillable = ['titulo', 'noticia', 'autor'];

    public function images(){
    	return $this->hasMany(ImageNoticia::class);
    }
}
