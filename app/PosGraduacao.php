<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosGraduacao extends Model
{
    protected $table = 'pos_graduacoes';
    protected $fillable = ['nome_pos','instituicao','grau','cidade','estado','url','data_fim_inscricao'];
    public $rules = [
    	'nome_pos' => 'required|min:10|max:255',
        'instituicao' => 'required|min:10|max:100',
    	'grau' => 'required',
    	'url' => 'required',
    	'data_fim_inscricao' => 'max:10'
    ];
    public $messages = [
    	'nome_pos.required' => 'Campo de preenchimento obrigatório',
    	'nome_pos.min' => 'O nome deve conter entre 10 e 255 caracteres',
    	'nome_pos.max' => 'O nome deve conter entre 10 e 255 caracteres',
    	'instituicao.required' => 'O nome da instituição é obrigatório',
        'instituicao.min' => 'O nome deve conter entre 3 e 100 caracteres',
        'instituicao.max' => 'O nome deve conter entre 3 e 100 caracteres',
        'url.required' => 'Você deve colocar um link para mais informações ou link de inscrição',
    	'grau.required' => 'Informe o grau da pós-graduação',
    	'data_fim_inscricao.max' => 'Data inválida! Por favor, verifique!'
    ];
}
