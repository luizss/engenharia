<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageEgresso extends Model
{
    protected $fillable = ['image','egresso_id'];
    //relacionamento one to one entre imagem e egresso
    public function egresso(){
    	return $this->belongsTo(Egresso::class);
    }

    public $rules = [
    	'image' => 'required',
    ];
    public $messages = [
    	'image.required' => 'Escolha uma imagem',
    ];
}
