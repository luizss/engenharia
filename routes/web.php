<?php

use Illuminate\Support\Facades\Route;

$this->get('nova-noticia', 'NoticiasController@cadNoticia');
Route::post('noticias/salvar','NoticiasController@store');
Route::get('noticias/lista-noticias','NoticiasController@lista')->name('lista-noticias');
Route::get('noticias/{noticia}/editar','NoticiasController@editar')->name('editar-noticia');
Route::patch('noticias/{news}','NoticiasController@atualizar');
Route::delete('noticias/{noticia}','NoticiasController@delete')->name('deletar-noticia');
Route::get('noticias/{new}/dados','NoticiasController@dadosNoticia')->name('dados-noticia');
Route::get('/', 'UsuariosController@index');
Route::get('noticias/{new}','UsuariosController@visualizar');

Route::group(['middleware' => 'web'], function () {
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
});
/*
 * rotas para egressos utilizando prefixos e grupos de rotas
 */
Route::prefix('egressos')->group(function(){
    Route::get('/','UsuariosController@egressos')->name('egressos');
    Route::get('/','EgressosController@index')->name('egressos-admin');
    Route::get('/novo','EgressosController@novo')->name('novo-egresso');
    Route::post('/salvar','EgressosController@salvar')->name('salvar-egresso');
    Route::get('/{egresso}/dados','EgressosController@dados')->name('dados-egresso');
    Route::get('/{egresso}/editar','EgressosController@editar')->name('editar-egresso');
    Route::patch('/{egresso}','EgressosController@alterar')->name('egresso-atualizar');
    Route::delete('/{egresso}','EgressosController@deletar')->name('egresso-deletar');
    Route::get('/{egresso}/detalhes','UsuariosController@viewEgresso')->name('egresso.detalhes');
});
Route::prefix('egressos/image')->group(function(){
    Route::get('/nova/{egresso}','ImageEgressosController@newImage')->name('egresso-nova-imagem');
    Route::post('/salvar','ImageEgressosController@salvar')->name('egresso-salvar-imagem');
});

Route::prefix('noticias/image')->group(function(){
    Route::get('/nova/{noticia}','ImageNoticiasController@nova')->name('noticia-nova-imagem');
    Route::post('/salvar','ImageNoticiasController@salvar')->name('noticia-salvar-imagem');
});

Route::prefix('posgraduacoes')->group(function(){
    Route::get('/list','PosGraduacoesController@index')->name('listapos');
    Route::get('/cadastro','PosGraduacoesController@novo')->name('poscadastro');
    Route::post('/salvar','PosGraduacoesController@salvar')->name('pos-salvar');
    Route::get('/{pos}/dados','PosGraduacoesController@dados')->name('dados-pos');
    Route::get('/{pos}/editar','PosGraduacoesController@editar')->name('pos-editar');
    Route::patch('/alterar/{pos}','PosGraduacoesController@alterar')->name('pos-alterar');
    Route::delete('/deletar/{pos}','PosGraduacoesController@deletar')->name('pos-deletar');
});

Route::prefix('eventos')->group(function(){
    Route::get('cadastrar','EventosController@novo')->name('cadastro-evento');
    Route::post('salvar','EventosController@salvar')->name('salvar-evento');
    Route::get('list','EventosController@lista')->name('lista-evento');
    Route::get('/{evento}/dados','EventosController@dados')->name('dados-evento');
    Route::get('/{evento}/editar','EventosController@editar')->name('editar-evento');
    Route::patch('/alterar/{evento}','EventosController@alterar')->name('alterar-evento');
    Route::delete('/{evento}','EventosController@excluir')->name('deletar-evento');
});